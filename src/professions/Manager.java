package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Manager extends Profession {
    
    public static int rate = 5;
    public static ProfessionType professionType = ProfessionType.Manager;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return true;
    }
}
