package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Accountant extends Profession {
    
    public static int rate = 6;
    public static ProfessionType professionType = ProfessionType.Accountant;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return true;
    }
}
