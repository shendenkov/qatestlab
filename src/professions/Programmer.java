package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Programmer extends Profession {
    
    public static int rate = 4;
    public static ProfessionType professionType = ProfessionType.Programmer;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return false;
    }
}
