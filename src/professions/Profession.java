package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public abstract class Profession {

    public static int rate = 1;
    public static ProfessionType professionType = ProfessionType.Cleaner;

    public enum ProfessionType {

        Director,
        Accountant,
        Manager,
        Programmer,
        Designer,
        Tester,
        Cleaner
    }

    public abstract ProfessionType getType();

    public abstract int getRate();

    public abstract boolean isFixedRate();
}
