package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Designer extends Profession {
    
    public static int rate = 3;
    public static ProfessionType professionType = ProfessionType.Designer;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return false;
    }
}
