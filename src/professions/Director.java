package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Director extends Profession {
    
    public static int rate = 7;
    public static ProfessionType professionType = ProfessionType.Director;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return true;
    }
}
