package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Cleaner extends Profession {
    
    public static final int rate = 1;
    public static final ProfessionType professionType = ProfessionType.Cleaner;
    
    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return false;
    }
}
