package professions;

/**
 *
 * @author Sergey Shendenkov
 */
public class Tester extends Profession {
    
    public static int rate = 2;
    public static ProfessionType professionType = ProfessionType.Tester;

    @Override
    public ProfessionType getType() {
        return professionType;
    }

    @Override
    public int getRate() {
        return rate;
    }

    @Override
    public boolean isFixedRate() {
        return false;
    }
}
