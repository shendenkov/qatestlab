package officework;

import Tasks.CleanOffice;
import Tasks.DrawMaket;
import Tasks.MakeReport;
import Tasks.SaleService;
import Tasks.Task;
import Tasks.Task.TaskType;
import Tasks.TestProgramm;
import Tasks.WriteCode;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.Random;
import java.util.logging.FileHandler;
import java.util.logging.Level;
import java.util.logging.Logger;
import professions.Accountant;
import professions.Director;
import professions.Manager;
import professions.Profession;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public class OfficeWork {

    public static final Logger sheduleReport = Logger.getLogger(OfficeWork.class.getName() + "_shedule");
    public static final Logger resultsReport = Logger.getLogger(OfficeWork.class.getName() + "_results");
    public static final Logger workReport = Logger.getLogger(OfficeWork.class.getName() + "_work");
    public static final Random rand = new Random();
    public static final String[] dayNames = new String[]{"SUN", "MON", "TUE", "WED", "THU", "FRI", "SAT"};
    public static final String[] monthNames = {"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    public static ArrayList<Task> tasks = new ArrayList<Task>();
    public static Calendar calendar = new GregorianCalendar();
    private static ArrayList<Employee> employees = new ArrayList<Employee>();
    private int currentDay = 0;
    private int currentMonth = 0;
    private int currentWeek = 0;
    private Employee freelance = null;
    private FileHandler sfh = null;
    private FileHandler rfh = null;
    private FileHandler wfh = null;

    public static void main(String[] args) throws IOException {
        OfficeWork office = new OfficeWork(rand.nextInt(91) + 10);
        office.work();
    }

    public OfficeWork(int employeesCount) throws IOException {
        init();
        for (int i = 0; i < employeesCount; i++) {
            employees.add(new Employee(i + 1, false));
        }
        freelance = new Employee(0, true);
        employees.add(freelance);
    }

    private void init() throws IOException {
        calendar.setFirstDayOfWeek(Calendar.MONDAY);
        calendar.set(Calendar.MONTH, rand.nextInt(12));
        calendar.set(Calendar.DAY_OF_MONTH, 1);
        calendar.set(Calendar.HOUR_OF_DAY, WorkDay.officeOpenHour);
        currentMonth = calendar.get(Calendar.MONTH);
        currentWeek = calendar.get(Calendar.WEEK_OF_YEAR);
        String sheduleFile = "shedule_report.txt";
        String resultsFile = "results_report.txt";
        String workFile = "work.txt";
        sheduleReport.setLevel(Level.ALL);
        resultsReport.setLevel(Level.ALL);
        workReport.setLevel(Level.ALL);
        sfh = new FileHandler(sheduleFile, 1024 * 1024 * 1024, 1, false);
        sfh.setFormatter(new SingleLineFormatter());
        sheduleReport.addHandler(sfh);
        rfh = new FileHandler(resultsFile, 1024 * 1024 * 1024, 1, false);
        rfh.setFormatter(new SingleLineFormatter());
        resultsReport.addHandler(rfh);
        wfh = new FileHandler(workFile, 1024 * 1024 * 1024, 1, false);
        wfh.setFormatter(new SingleLineFormatter());
        workReport.addHandler(wfh);
        sheduleReport.log(Level.FINE, "{0} {1}", new Object[]{monthNames[currentMonth], calendar.get(Calendar.YEAR)});
        resultsReport.log(Level.FINE, "{0} {1}", new Object[]{monthNames[calendar.get(Calendar.MONTH)], calendar.get(Calendar.YEAR)});
        workReport.log(Level.FINE, "{0} {1}", new Object[]{monthNames[calendar.get(Calendar.MONTH)], calendar.get(Calendar.YEAR)});
        sheduleReport.log(Level.FINE, "");
        resultsReport.log(Level.FINE, "");
        workReport.log(Level.FINE, "");
    }

    public static ProfessionType getCriticalProfessionWanted() {
        ProfessionType professionType = null;
        boolean stop = false;
        boolean haveDirector = false;
        boolean haveAccountant = false;
        boolean haveManager = false;
        for (Employee employee : employees) {
            for (Profession profession : employee.professions) {
                if (profession instanceof Director) {
                    haveDirector = true;
                } else if (profession instanceof Accountant) {
                    haveAccountant = true;
                } else if (profession instanceof Manager) {
                    haveManager = true;
                }
                if (haveDirector && haveAccountant && haveManager) {
                    stop = true;
                    break;
                }
            }
            if (stop) {
                break;
            }
        }
        if (!haveDirector) {
            professionType = ProfessionType.Director;
        } else if (!haveAccountant) {
            professionType = ProfessionType.Accountant;
        } else if (!haveManager) {
            professionType = ProfessionType.Manager;
        }
        return professionType;
    }

    private void work() {
        int directorsAtWork = 0;
        while (calendar.get(Calendar.MONTH) == currentMonth) {
            if (calendar.get(Calendar.DAY_OF_MONTH) != currentDay) {
                currentDay = calendar.get(Calendar.DAY_OF_MONTH);
                calendar.set(Calendar.HOUR_OF_DAY, WorkDay.officeOpenHour);
                workReport.log(Level.FINE, "Day {0} {1} {2} - {3} hours:",
                        new Object[]{currentDay, dayNames[calendar.get(Calendar.DAY_OF_WEEK) - 1], calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.HOUR_OF_DAY) + 1});
            } else if (calendar.get(Calendar.HOUR_OF_DAY) < WorkDay.officeCloseHour) {
                workReport.log(Level.FINE, "{0} - {1} hours:", new Object[]{calendar.get(Calendar.HOUR_OF_DAY), calendar.get(Calendar.HOUR_OF_DAY) + 1});
            }
            if (isWorkNow()) {
                if (currentWeek != calendar.get(Calendar.WEEK_OF_YEAR) && getEmployeesAtWorkCount(Accountant.class) > 0) {
                    for (Employee employee : employees) {
                        if (!employee.isFreelance()) {
                            employee.paySalary();
                        }
                    }
                }
                directorsAtWork = getEmployeesAtWorkCount(Director.class);
                if (directorsAtWork > 0) {
                    workReport.log(Level.FINE, "New tasks:");
                    for (int i = 0; i < directorsAtWork; i++) {
                        generateNewTasks(rand.nextInt(getEmployeesAtWorkCount(null)));
                    }
                }
                workReport.log(Level.FINE, "Work:");
                for (Employee employee : employees) {
                    if (!employee.isFreelance() && employee.isWorkNow()) {
                        employee.doWork();
                    }
                }
                freelance.createFreelanceTasks();
            }
            freelance.doWork();
            freelance.paySalary();
            calendar.add(Calendar.HOUR_OF_DAY, 1);
        }
        for (Employee employee : employees) {
            employee.paySalary();
        }
        makeReport();
        close();
    }

    public static boolean isWorkNow() {
        return (calendar.get(Calendar.HOUR_OF_DAY) >= WorkDay.officeOpenHour && calendar.get(Calendar.HOUR_OF_DAY) < WorkDay.officeCloseHour);
    }

    private int getEmployeesAtWorkCount(Class<?> profession) {
        int result = 0;
        for (Employee employee : employees) {
            for (int i = 0; i < employee.professions.size(); i++) {
                if (employee.isWorkNow() && (profession == null || profession.isInstance(employee.professions.get(i)))) {
                    result++;
                }
            }
        }
        return result;
    }

    private void generateNewTasks(int tasksCount) {
        Task task = null;
        TaskType taskType = null;
        for (int i = 0; i < tasksCount; i++) {
            taskType = TaskType.values()[rand.nextInt(6)];
            if (taskType == TaskType.WriteCode) {
                task = new WriteCode();
            } else if (taskType == TaskType.DrawMaket) {
                task = new DrawMaket();
            } else if (taskType == TaskType.TestProgramm) {
                task = new TestProgramm();
            } else if (taskType == TaskType.SellService) {
                task = new SaleService();
            } else if (taskType == TaskType.MakeReport) {
                task = new MakeReport();
            } else if (taskType == TaskType.CleanOffice) {
                task = new CleanOffice();
            }
            task.setPriority(rand.nextInt(100));
            task.setTime(rand.nextInt(2) + 1);
            workReport.log(Level.FINE, "{0} (priority {1}, duration {2} hours)", new Object[]{task.getTaskType(), task.getPriority(), task.getTime()});
            tasks.add(task);
        }
    }

    private void makeReport() {
        int salaryPaid = 0;
        int countWriteCode = 0;
        int countDrawMaket = 0;
        int countTestProgramm = 0;
        int countSellService = 0;
        int countMakeReport = 0;
        int countCleanOffice = 0;
        for (Employee employee : employees) {
            employee.paySalary();
            salaryPaid += employee.getBalance();
            resultsReport.log(Level.FINE, "{0} received salary = ${1}", new Object[]{employee.getName(), employee.getBalance()});
            countCleanOffice += employee.getCountCleanOffice();
            countDrawMaket += employee.getCountDrawMaket();
            countMakeReport += employee.getCountMakeReport();
            countSellService += employee.getCountSellService();
            countTestProgramm += employee.getCountTestProgramm();
            countWriteCode += employee.getCountWriteCode();
            if (employee.getCountCleanOffice() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.CleanOffice, employee.getCountCleanOffice()});
            }
            if (employee.getCountDrawMaket() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.DrawMaket, employee.getCountDrawMaket()});
            }
            if (employee.getCountMakeReport() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.MakeReport, employee.getCountMakeReport()});
            }
            if (employee.getCountSellService() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.SellService, employee.getCountSellService()});
            }
            if (employee.getCountTestProgramm() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.TestProgramm, employee.getCountTestProgramm()});
            }
            if (employee.getCountWriteCode() > 0) {
                resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.WriteCode, employee.getCountWriteCode()});
            }
            resultsReport.log(Level.FINE, "");
        }
        resultsReport.log(Level.FINE, "Total:");
        resultsReport.log(Level.FINE, "Salary paid = ${0}", salaryPaid);
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.CleanOffice, countCleanOffice});
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.DrawMaket, countDrawMaket});
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.MakeReport, countMakeReport});
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.SellService, countSellService});
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.TestProgramm, countTestProgramm});
        resultsReport.log(Level.FINE, "done {0} {1} times", new Object[]{TaskType.WriteCode, countWriteCode});
    }
    
    private void close() {
        sfh.close();
        rfh.close();
        wfh.close();
    }
}
