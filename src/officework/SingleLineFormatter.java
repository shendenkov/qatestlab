package officework;

import java.util.logging.Formatter;
import java.util.logging.LogRecord;

/**
 *
 * @author Sergey Shendenkov
 */
public final class SingleLineFormatter extends Formatter {

    @Override
    public String format(LogRecord record) {
        StringBuilder sb = new StringBuilder();
        sb.append(formatMessage(record)).append("\n");
        System.out.println(formatMessage(record));
        return sb.toString();
    }
}
