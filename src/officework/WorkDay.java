package officework;

/**
 *
 * @author Sergey Shendenkov
 */
public class WorkDay {

    public static int officeOpenHour = 8;
    public static int officeCloseHour = 20;
    private int startWorkHour = -1;
    private int endWorkHour = -1;
    private int lunchHour = -1;
    private int workHours = 0;

    public WorkDay(int workHours, int startWorkHour, int lunchWork) {
        this.workHours = workHours;
        this.startWorkHour = startWorkHour;
        this.lunchHour = lunchWork;
        endWorkHour = startWorkHour + workHours;
        if (lunchWork != -1) {
            endWorkHour++;
        }
    }

    public boolean isWithLunch() {
        return lunchHour != -1;
    }

    public int getLunchHour() {
        return lunchHour;
    }

    public void setLunchHour(int lunchHour) {
        this.lunchHour = lunchHour;
    }

    public int getStartWorkHour() {
        return startWorkHour;
    }

    public void setStartWorkHour(int startWorkHour) {
        this.startWorkHour = startWorkHour;
    }

    public int getEndWorkHour() {
        return endWorkHour;
    }

    public void setEndWorkHour(int endWorkHour) {
        this.endWorkHour = endWorkHour;
    }

    public int getWorkHours() {
        return workHours;
    }

    public void setWorkHours(int workHours) {
        this.workHours = workHours;
    }
}
