package officework;

import Tasks.CleanOffice;
import Tasks.DrawMaket;
import Tasks.MakeReport;
import Tasks.SaleService;
import Tasks.Task;
import Tasks.TestProgramm;
import Tasks.WriteCode;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.logging.Level;
import professions.Accountant;
import professions.Cleaner;
import professions.Designer;
import professions.Director;
import professions.Manager;
import professions.Profession;
import professions.Profession.ProfessionType;
import professions.Programmer;
import professions.Tester;

/**
 *
 * @author Sergey Shendenkov
 */
public class Employee {

    public ArrayList<Profession> professions = new ArrayList<Profession>();
    private boolean director = false;
    private boolean freelance = false;
    private int balance = 0;
    private int countWriteCode = 0;
    private int countDrawMaket = 0;
    private int countTestProgramm = 0;
    private int countSellService = 0;
    private int countMakeReport = 0;
    private int countCleanOffice = 0;
    private int fixedProfit = 0;
    private int number = 0;
    private ArrayList<Task> doneTasks = new ArrayList<Task>();
    private ArrayList<Task> performTasks = new ArrayList<Task>();
    private String name = null;
    private WorkDay[] shedule = new WorkDay[7];

    public Employee(int number, boolean freelance) {
        this.number = number;
        this.freelance = freelance;
        if (freelance) {
            name = "Freelancers";
        } else {
            OfficeWork.sheduleReport.log(Level.FINE, "Employee {0}", number);
            generateProfessions();
            generateShedule();
        }
    }

    private void generateProfessions() {
        ProfessionType professionType = null;
        int maxProfessions = OfficeWork.rand.nextInt(4) + 1;
        name = "Employee " + number + " (";
        StringBuilder str = new StringBuilder();
        StringBuilder logMsg = new StringBuilder("Professions: ");
        for (int i = 0; i < maxProfessions; i++) {
            professionType = OfficeWork.getCriticalProfessionWanted();
            if (professionType == null) {
                professionType = ProfessionType.values()[OfficeWork.rand.nextInt(7)];
                if (isAlreadyHaveSuchProfession(professionType)) {
                    i--;
                    continue;
                }
            }
            if (professionType == ProfessionType.Director) {
                if (professions.isEmpty()) {
                    professions.add(new Director());
                    str.append("Director");
                    director = true;
                    if (maxProfessions > 1) {
                        professions.add(new Manager());
                        str.append(", Manager");
                    }
                    break;
                } else if (professions.size() == 1 && professions.get(0) instanceof Manager) {
                    professions.add(new Director());
                    str.append("Director");
                    director = true;
                    break;
                } else {
                    i--;
                    continue;
                }
            } else if (professionType == ProfessionType.Accountant) {
                if (professions.isEmpty()) {
                    professions.add(new Accountant());
                    str.append("Accountant");
                    if (maxProfessions > 1) {
                        professions.add(new Manager());
                        str.append(", Manager");
                    }
                    break;
                } else if (professions.size() == 1 && professions.get(0) instanceof Manager) {
                    professions.add(new Accountant());
                    str.append("Accountant");
                    break;
                } else {
                    i--;
                    continue;
                }
            } else if (professionType == ProfessionType.Manager) {
                professions.add(new Manager());
            } else if (professionType == ProfessionType.Programmer) {
                professions.add(new Programmer());
            } else if (professionType == ProfessionType.Designer) {
                professions.add(new Designer());
            } else if (professionType == ProfessionType.Tester) {
                professions.add(new Tester());
            } else if (professionType == ProfessionType.Cleaner && professions.isEmpty()) {
                professions.add(new Cleaner());
                str.append("Cleaner");
                break;
            } else {
                i--;
                continue;
            }
            str.append(professionType);
            if (i + 1 < maxProfessions) {
                str.append(", ");
            }
        }
        name += str + ")";
        logMsg.append(str);
        OfficeWork.sheduleReport.log(Level.FINE, logMsg.toString());
    }

    private boolean isAlreadyHaveSuchProfession(ProfessionType professionType) {
        boolean result = false;
        for (Profession profession : professions) {
            if (professionType == profession.getType()) {
                result = true;
                profession = null;
                break;
            }
            profession = null;
        }
        return result;
    }

    private void generateShedule() {
        int startWorkHour = 0;
        int dayWorkHours = 0;
        int weekHours = 0;
        int lunchHour = -1;
        WorkDay workDay = null;
        StringBuilder logMsg = new StringBuilder("Shedule: \n");
        for (int i = 0; i < shedule.length; i++) {
            lunchHour = -1;
            startWorkHour = OfficeWork.rand.nextInt(WorkDay.officeCloseHour - WorkDay.officeOpenHour) + WorkDay.officeOpenHour;
            dayWorkHours = OfficeWork.rand.nextInt(9);
            if (startWorkHour + dayWorkHours > WorkDay.officeCloseHour) {
                dayWorkHours = WorkDay.officeCloseHour - startWorkHour;
            }
            if (weekHours + dayWorkHours > 40) {
                dayWorkHours = 40 - weekHours;
            }
            if (dayWorkHours > 5) {
                lunchHour = dayWorkHours / 2;
                if (startWorkHour + dayWorkHours + 1 > WorkDay.officeCloseHour) {
                    dayWorkHours = WorkDay.officeCloseHour - startWorkHour;
                }
            }
            workDay = new WorkDay(dayWorkHours, startWorkHour, lunchHour);
            shedule[i] = workDay;
            weekHours += dayWorkHours;
            logMsg.append(OfficeWork.dayNames[(i + 1 < 7 ? i + 1 : 0)]).append(" = ").append(workDay.getWorkHours()).append(" hours");
            if (dayWorkHours > 0) {
                logMsg.append(" from ").append(workDay.getStartWorkHour()).append(" to ").append(workDay.getEndWorkHour());
                if (lunchHour != -1) {
                    logMsg.append(" with lunch from ").append(workDay.getLunchHour()).append(" to ").append(workDay.getLunchHour() + 1);
                }
            }
            if (i + 1 < 7) {
                logMsg.append(", \n");
            }
        }
        logMsg.append("\nTotal: ").append(weekHours).append(" hours on week\n");
        OfficeWork.sheduleReport.log(Level.FINE, logMsg.toString());
    }

    public void doWork() {
        int productivity = 1;
        if (freelance) {
            productivity = performTasks.size();
        } else {
            findPriorityTask();
        }
        if (!performTasks.isEmpty()) {
            Task task = null;
            for (int i = 0; i < productivity; i++) {
                task = performTasks.get(i);
                task.doTask();
                if (OfficeWork.isWorkNow() && task.getTime() < 1) {
                    OfficeWork.workReport.log(Level.FINE, "{0} finished {1} (priority {2})", new Object[]{name, task.getTaskType(), task.getPriority()});
                    OfficeWork.tasks.remove(task);
                    performTasks.remove(task);
                    doneTasks.add(task);
                    countTask(task);
                    productivity--;
                    i--;
                }
            }
        } else {
            if (!director && !freelance) {
                OfficeWork.workReport.log(Level.FINE, "{0} is idling", name);
            }
        }
        for (Profession profession : professions) {
            if (profession.isFixedRate()) {
                if (OfficeWork.calendar.get(Calendar.DAY_OF_WEEK) > 5) {
                    fixedProfit += profession.getRate() * 2;
                } else {
                    fixedProfit += profession.getRate();
                }
            }
        }
    }

    public boolean isWorkNow() {
        boolean result = false;
        Calendar calendar = OfficeWork.calendar;
        int day = calendar.get(Calendar.DAY_OF_WEEK) - 1;
        if (day == 0) {
            day = 7;
        }
        if (calendar.get(Calendar.HOUR_OF_DAY) >= shedule[day - 1].getStartWorkHour() && calendar.get(Calendar.HOUR_OF_DAY) < shedule[day - 1].getEndWorkHour()) {
            if (!shedule[day - 1].isWithLunch() || calendar.get(Calendar.HOUR_OF_DAY) != shedule[day - 1].getLunchHour()) {
                result = true;
            }
        }
        return result;
    }

    public void createFreelanceTasks() {
        for (Task task : OfficeWork.tasks) {
            if (!task.isPerformedNow() && !(task instanceof CleanOffice)) {
                task.setPerformedNow(true);
                performTasks.add(task);
                OfficeWork.workReport.log(Level.FINE, "{0} start {1} (priority {2}, duration {3} hours). In query {4} tasks",
                        new Object[]{name, task.getTaskType(), task.getPriority(), task.getTime(), performTasks.size() - 1});
            }
        }
    }

    private void findPriorityTask() {
        boolean found = false;
        Task mostPriorityTask = null;
        if (!performTasks.isEmpty()) {
            mostPriorityTask = performTasks.get(0);
        }
        for (Task task : OfficeWork.tasks) {
            if (!task.isPerformedNow()) {
                for (Profession profession : professions) {
                    if (profession.getType() == task.getProfessionTypeNeed()) {
                        if (mostPriorityTask == null || mostPriorityTask.getPriority() < task.getPriority()
                                || (mostPriorityTask.getPriority() == task.getPriority() && mostPriorityTask.getRate() < task.getRate())) {
                            mostPriorityTask = task;
                            found = true;
                        }
                    }
                }
            }
        }
        if (found) {
            mostPriorityTask.setPerformedNow(true);
            performTasks.add(0, mostPriorityTask);
            OfficeWork.workReport.log(Level.FINE, "{0} start {1} (priority {2}, duration {3} hours). In query {4} tasks",
                    new Object[]{name, mostPriorityTask.getTaskType(), mostPriorityTask.getPriority(), mostPriorityTask.getTime(), performTasks.size() - 1});
        }
    }

    private void countTask(Task task) {
        if (task instanceof WriteCode) {
            countWriteCode++;
        } else if (task instanceof DrawMaket) {
            countDrawMaket++;
        } else if (task instanceof TestProgramm) {
            countTestProgramm++;
        } else if (task instanceof SaleService) {
            countSellService++;
        } else if (task instanceof MakeReport) {
            countMakeReport++;
        } else if (task instanceof CleanOffice) {
            countCleanOffice++;
        }
    }

    public boolean isFreelance() {
        return freelance;
    }

    public void paySalary() {
        for (Task task : doneTasks) {
            balance += task.pay();
        }
        for (Task task : performTasks) {
            balance += task.pay();
        }
        balance += fixedProfit;
        fixedProfit = 0;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    public int getCountCleanOffice() {
        return countCleanOffice;
    }

    public int getCountDrawMaket() {
        return countDrawMaket;
    }

    public int getCountMakeReport() {
        return countMakeReport;
    }

    public int getCountSellService() {
        return countSellService;
    }

    public int getCountTestProgramm() {
        return countTestProgramm;
    }

    public int getCountWriteCode() {
        return countWriteCode;
    }
}
