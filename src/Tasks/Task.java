package Tasks;

import java.util.Calendar;
import officework.OfficeWork;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public abstract class Task {

    public boolean perform = false;
    public int priority = 0;
    public int profit = 0;
    public int time = -1;

    public enum TaskType {

        WriteCode,
        DrawMaket,
        TestProgramm,
        SellService,
        MakeReport,
        CleanOffice
    }

    public abstract TaskType getTaskType();

    public abstract ProfessionType getProfessionTypeNeed();

    public abstract int getRate();

    public void doTask() {
        time--;
        if (getProfessionTypeNeed() != ProfessionType.Director && getProfessionTypeNeed() != ProfessionType.Accountant && getProfessionTypeNeed() != ProfessionType.Manager) {
            if (OfficeWork.calendar.get(Calendar.DAY_OF_WEEK) > 5) {
                profit += getRate() * 2;
            } else {
                profit += getRate();
            }
        }
    }

    public int pay() {
        int result = profit;
        profit = 0;
        return result;
    }

    public void setPerformedNow(boolean perform) {
        this.perform = perform;
    }

    public boolean isPerformedNow() {
        return perform;
    }

    public void setPriority(int priority) {
        this.priority = priority;
    }

    public int getPriority() {
        return priority;
    }

    public void setTime(int time) {
        this.time = time;
    }

    public int getTime() {
        return time;
    }
}
