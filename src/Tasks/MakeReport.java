package Tasks;

import professions.Accountant;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public class MakeReport extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.MakeReport;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Accountant.professionType;
    }

    @Override
    public int getRate() {
        return Accountant.rate;
    }
}
