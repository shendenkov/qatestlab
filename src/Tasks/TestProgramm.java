package Tasks;

import professions.Profession.ProfessionType;
import professions.Tester;

/**
 *
 * @author Sergey Shendenkov
 */
public class TestProgramm extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.TestProgramm;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Tester.professionType;
    }

    @Override
    public int getRate() {
        return Tester.rate;
    }
}
