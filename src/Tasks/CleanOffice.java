package Tasks;

import professions.Cleaner;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public class CleanOffice extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.CleanOffice;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Cleaner.professionType;
    }

    @Override
    public int getRate() {
        return Cleaner.rate;
    }
}
