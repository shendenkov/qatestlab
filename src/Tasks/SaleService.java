package Tasks;

import professions.Manager;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public class SaleService extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.SellService;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Manager.professionType;
    }

    @Override
    public int getRate() {
        return Manager.rate;
    }
}
