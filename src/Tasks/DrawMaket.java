package Tasks;

import professions.Designer;
import professions.Profession.ProfessionType;

/**
 *
 * @author Sergey Shendenkov
 */
public class DrawMaket extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.DrawMaket;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Designer.professionType;
    }

    @Override
    public int getRate() {
        return Designer.rate;
    }
}
