package Tasks;

import professions.Profession.ProfessionType;
import professions.Programmer;

/**
 *
 * @author Sergey Shendenkov
 */
public class WriteCode extends Task {

    @Override
    public TaskType getTaskType() {
        return TaskType.WriteCode;
    }

    @Override
    public ProfessionType getProfessionTypeNeed() {
        return Programmer.professionType;
    }

    @Override
    public int getRate() {
        return Programmer.rate;
    }
}
